@extends('master')

@section('content')

<table class="table mt-3 ml-3 mr-3">
    @if (session('success'))
        <div class="alert alert-success mt-3 ml-3 mr-3">
            {{ session('success') }}
        </div>
    @endif
    <form class="mb-3">
        <a class="btn btn-primary mt-3 ml-3" href="pertanyaan/create">Create</a>
    </form>
    <thead class="thead-light">
        <tr>
        <th scope="col">NO</th>
        <th scope="col">Judul</th>
        <th scope="col">Isi</th>
        <th scope="col">Aksi</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($pertanyaan as $key =>$pertanyaan)
            <tr>
                <td> {{ $key + 1 }}</td>
                <td> {{ $pertanyaan->judul }} </td>
                <td> {{ $pertanyaan->isi }} </td>
                <td style="display: flex">
                    <a href="/pertanyaan/{{ $pertanyaan->id }}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/pertanyaan/{{ $pertanyaan->id }}/edit" class="btn btn-primary btn-sm">Edit</a>
                    <form action="/pertanyaan/{{ $pertanyaan->id }}" method="post">
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="delete" class="btn btn-danger btn-sm">
                    </form>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

@endsection